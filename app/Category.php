<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use \Dimsav\Translatable\Translatable;

    public $translatedAttributes = ['cat_name','cat_title','cat_content'];
    protected $fillable = ['featured_image'];
}
