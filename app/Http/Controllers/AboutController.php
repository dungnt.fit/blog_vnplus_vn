<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function showAbout(){
        $product = Product::all();
        return view('_pages/about',compact('product'));
    }
    public function changeLanguage($language)
    {
        \Session::put('website_language', $language);

        return redirect()->back();
    }
}
