<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::all();
        return view('admin.category.list',compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category();
        foreach(LaravelLocalization::getSupportedLocales() as $locale => $value) {
            $category->translateOrNew($locale)->cat_name = $request->input('cat_name')[$locale];
            $category->translateOrNew($locale)->cat_title = $request->input('cat_title')[$locale];
            $category->translateOrNew($locale)->cat_content = $request->input('cat_content')[$locale];
        }

        $category->save();

        $notification = array(
            'message' => 'Add category successfully!',
            'alert-type' => 'success'
        );
        return redirect()->route('category.list')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::all()->find($id);
        return view('admin.category.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::FindOrFail($id);
        foreach(LaravelLocalization::getSupportedLocales() as $locale => $value) {
            $category->translateOrNew($locale)->cat_name = $request->input('cat_name')[$locale];
            $category->translateOrNew($locale)->cat_title = $request->input('cat_title')[$locale];
            $category->translateOrNew($locale)->cat_content = $request->input('cat_content')[$locale];
        }

        $category->save();

        $notification = array(
            'message' => 'Add category successfully!',
            'alert-type' => 'success'
        );
        return redirect()->route('category.list')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::FindOrFail($id);
        $category->delete();

        $notification = array(
            'message' => 'Delete category successfully!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

}
