<?php

namespace App\Http\Controllers\Admin;

use App\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::all();
        return view('admin.news.list',compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $news = new News();
        foreach(LaravelLocalization::getSupportedLocales() as $locale => $value) {
            $news->translateOrNew($locale)->news_name = $request->input('news_name')[$locale];
            $news->translateOrNew($locale)->news_title = $request->input('news_title')[$locale];
            $news->translateOrNew($locale)->news_content = $request->input('news_content')[$locale];
            $news->translateOrNew($locale)->news_rewrite = changeTitle($request->input('news_name')[$locale]) .'/news'. $news->id;
        }


        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/assets/images/upload_image');
            $image->move($destinationPath, $name);
            $news->featured_image = $name;

        }

        $news->save();

        $notification = array(
            'message' => 'Add news successfully!',
            'alert-type' => 'success'
        );
        return redirect()->route('news.list')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::all()->find($id);
        return view('admin.news.edit',compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $news = News::FindOrFail($id);
        foreach(LaravelLocalization::getSupportedLocales() as $locale => $value) {
            $news->translateOrNew($locale)->news_name = $request->input('news_name')[$locale];
            $news->translateOrNew($locale)->news_title = $request->input('news_title')[$locale];
            $news->translateOrNew($locale)->news_content = $request->input('news_content')[$locale];
            $news->translateOrNew($locale)->news_rewrite = changeTitle($request->input('news_name')[$locale]) .'/news'. $news->id;
        }



        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/assets/images/upload_image');
            $image->move($destinationPath, $name);
            $news->featured_image = $name;

        }

        $news->save();

        $notification = array(
            'message' => 'Save news successfully!',
            'alert-type' => 'success'
        );
        return redirect()->route('news.list')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::FindOrFail($id);
        $news->delete();

        $notification = array(
            'message' => 'Delete news successfully!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
}
