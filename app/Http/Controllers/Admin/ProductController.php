<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\ProductTranslation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::all();
        return view('admin.product.list',compact('product'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new Product();
        foreach(LaravelLocalization::getSupportedLocales() as $locale => $value) {
            $product->translateOrNew($locale)->name = $request->input('name')[$locale];
            $product->translateOrNew($locale)->excerpt = $request->input('excerpt')[$locale];
            $product->translateOrNew($locale)->description = $request->input('description')[$locale];
            $product->translateOrNew($locale)->content = $request->input('content')[$locale];
            $product->translateOrNew($locale)->pro_rewrite = changeTitle($request->input('name')[$locale]);
            $product->translateOrNew($locale)->pro_info = $request->input('info')[$locale];
        }
        $product->img_name = changeTitle($request->input('name')['en']);


        if($request->hasFile('image')) {
            $allowedfileExtension = ['jpg', 'png'];
            $files = $request->file('image');
            // flag xem có thực hiện lưu DB không. Mặc định là có
            $exe_flg = true;
            // kiểm tra tất cả các files xem có đuôi mở rộng đúng không
            foreach ($files as $file) {
                $extension = $file->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);

                if (!$check) {
                    // nếu có file nào không đúng đuôi mở rộng thì đổi flag thành false
                    $exe_flg = false;
                    break;
                }
            }
            // nếu không có file nào vi phạm validate thì tiến hành lưu DB
            if ($exe_flg) {
                // duyệt từng ảnh và thực hiện lưu
                $arr_names = [];
                foreach ($request->image as $photo) {

                    $filename =$photo->store('photos');
                    $arr_names[] = substr($filename,7);
                }

                $product->featured_image = json_encode($arr_names);
            }
        }

        $product->save();

        $notification = array(
            'message' => 'Add product successfully!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::all()->find($id);
        return view('admin.product.edit',compact('product','product_translate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::FindOrFail($id);
        foreach(LaravelLocalization::getSupportedLocales() as $locale => $value) {
            $product->translateOrNew($locale)->name = $request->input('name')[$locale];
            $product->translateOrNew($locale)->excerpt = $request->input('excerpt')[$locale];
            $product->translateOrNew($locale)->description = $request->input('description')[$locale];
            $product->translateOrNew($locale)->content = $request->input('content')[$locale];
            $product->translateOrNew($locale)->pro_rewrite = changeTitle($request->input('name')[$locale]);
            $product->translateOrNew($locale)->pro_info = $request->input('pro_info')[$locale];
        }
        $product->img_name = changeTitle($request->input('name')['en']);

        if($request->hasFile('image')) {
            $allowedfileExtension = ['jpg', 'png'];
            $files = $request->file('image');
            // flag xem có thực hiện lưu DB không. Mặc định là có
            $exe_flg = true;
            // kiểm tra tất cả các files xem có đuôi mở rộng đúng không
            foreach ($files as $file) {
                $extension = $file->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);

                if (!$check) {
                    // nếu có file nào không đúng đuôi mở rộng thì đổi flag thành false
                    $exe_flg = false;
                    break;
                }
            }
            // nếu không có file nào vi phạm validate thì tiến hành lưu DB
            if ($exe_flg) {
                // duyệt từng ảnh và thực hiện lưu
                $arr_names = [];
                foreach ($request->image as $photo) {
                    $filename = $photo->store('photos');
                    $arr_names[] = substr($filename,7);
                }

                $product->featured_image = json_encode($arr_names);
            }
        }

        $product->save();


        $notification = array(
            'message' => 'Save product successfully!',
            'alert-type' => 'success'
        );
        return redirect()->route('product.list')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::FindOrFail($id);
        $product->delete();

        $notification = array(
            'message' => 'Delete product successfully!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
}
