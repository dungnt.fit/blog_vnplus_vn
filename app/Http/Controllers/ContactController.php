<?php

namespace App\Http\Controllers;

use App\Mail\ContactsMail;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function showContact(){
        $product = Product::all();
        return view('_pages/contact',compact('product'));
    }
    public function  store(Request $request){
        Mail::send(new ContactsMail($request));
        return redirect('/');
    }
}
