<?php

namespace App\Http\Controllers;

use App\Category;
use App\News;
use App\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $product = Product::limit(5)->get();
        $category = Category::all();
        $news = News::limit(5)->orderBy('id','desc')->get();
        return view('_pages.home',compact('product','news','category'));
    }
}
