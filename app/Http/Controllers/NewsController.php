<?php

namespace App\Http\Controllers;

use App\News;
use App\Product;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function showNews(){
        $news = News::orderBy('id','desc')->get();
        $product = Product::all();
        return view('_pages.news',compact('product','news'));
    }
}
