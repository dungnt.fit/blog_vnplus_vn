<?php

namespace App\Http\Controllers;

use App\News;
use App\Product;
use Illuminate\Http\Request;

class NewsDetailController extends Controller
{
    public function index($id){
        $product = Product::all();
        $news_detail = News::all()->find($id);
        return view('_pages.news_detail',compact('news_detail','product'));
    }
}
