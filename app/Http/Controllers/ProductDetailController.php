<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductDetailController extends Controller
{
    public function index($id){
        $product = Product::all();
        $pro_detail = Product::all()->find($id);
        return view('_pages.pro_detail',compact('pro_detail','product'));
    }
}
