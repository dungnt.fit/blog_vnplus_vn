<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function showProducts(){
        $product = Product::all();
        return view('_pages/products',compact('product'));
    }
}
