<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use \Dimsav\Translatable\Translatable;

    public $translatedAttributes = ['news_name','news_title','news_content','news_rewrite'];
    protected $fillable = ['featured_image'];
}
