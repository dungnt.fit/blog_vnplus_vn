<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use \Dimsav\Translatable\Translatable;

    public $translatedAttributes = ['name','excerpt','description','content','image','pro_rewrite','pro_info'];
    protected $fillable = ['featured_image'];
}
