<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('featured_image')->nullable();
            $table->timestamps();
        });

        Schema::create('product_translations', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('product_id')->unsigned(); //macdinh

            $table->string('name')->nullable(); //truong can dich
            $table->text('excerpt')->nullable(); //truong can dich
            $table->text('description')->nullable();//truong can dich
            $table->longText('content')->nullable();//truong can dichphp

            $table->string('locale')->index(); //macdinh

            $table->unique(['product_id','locale']);//macdinh
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');//macdinh

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_translations');
        Schema::dropIfExists('products');
    }
}
