<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('featured_image')->nullable();
            $table->timestamps();
        });

        Schema::create('category_translations', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('cat_id')->unsigned(); //macdinh

            $table->string('cat_name')->nullable(); //truong can dich
            $table->text('cat_title')->nullable();//truong can dich
            $table->longText('cat_content')->nullable();//truong can dich

            $table->string('locale')->index(); //macdinh

            $table->unique(['cat_id','locale']);//macdinh
            $table->foreign('cat_id')->references('id')->on('categories')->onDelete('cascade');//macdinh

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_translations');
        Schema::dropIfExists('categories');
    }
}
