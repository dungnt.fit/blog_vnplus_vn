<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('featured_image')->nullable();
            $table->timestamps();
        });

        Schema::create('news_translations', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('news_id')->unsigned(); //macdinh

            $table->string('news_name')->nullable(); //truong can dich
            $table->text('news_title')->nullable();//truong can dich
            $table->longText('news_content')->nullable();//truong can dich

            $table->string('locale')->index(); //macdinh

            $table->unique(['news_id','locale']);//macdinh
            $table->foreign('news_id')->references('id')->on('news')->onDelete('cascade');//macdinh

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_translations');
        Schema::dropIfExists('news');
    }
}
