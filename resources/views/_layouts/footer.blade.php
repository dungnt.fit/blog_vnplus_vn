<footer>
    <section class="sec-6">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="txt_sec6">
                        <h4>{{trans('test.headquarter')}}</h4>
                        <ul>
                            <li>{{trans('test.detail-headquarter')}}</li>
                            <li>{{trans('test.phone')}}: +84 (24) 3201 1329 </li>
                            <li>Fax: +84 (24) 3201 1329</li>
                            <li>Email: vnplus@vnplusvn.com</li>
                            <li>Website: www.vnplusvn.com</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="txt_sec6">
                        <h4>{{trans('test.factory')}}</h4>
                        <ul>
                            <li>{{trans('test.factory-1')}}</li>
                            <li>{{trans('test.factory-2')}}</li>
                            <li>{{trans('test.factory-3')}}</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="txt_sec6">
                        <h4>{{trans('test.our-certificates')}}</h4>
                        <ul>
                            <li>{{trans('test.Certificate-1')}}</li>
                            <li>{{trans('test.Certificate-2')}}</li>
                            <li>{{trans('test.Certificate-3')}}</li>
                            <li>{{trans('test.Certificate-4')}}</li>
                            <li>{{trans('test.Certificate-5')}}</li>
                            <li>{{trans('test.Certificate-6')}}</li>
                            <li>{{trans('test.Certificate-7')}}</li>

                        </ul>
                    </div>
                </div>
            </div>
            <hr>
            <div class="txt_sec6a">
                <h5>© 2018 VNPLUS Import & Export . JSC . All rights reserved</h5>
            </div>
        </div>
    </section>
</footer>