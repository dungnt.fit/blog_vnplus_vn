
<header>
    <nav class="navbar navbar-expand-md bg-dark navbar-dark">
        <div class="logo">
            <a class="navbar-brand" href="{{route('home')}}">
                <img src="assets/images/logo/logo-vnplus.png" width="170px" height="200px" alt="">
            </a>
        </div>
        <div class="logo-mobile">
            <a class="navbar-brand" href="#">
                <img src="assets/images/logo/logo-vnplus.png" width="70px" height="70px" alt="">
            </a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="nav-item fix-menu">
                    <a class="nav-link" href="{{route('home')}}">{{trans('test.home')}}</a>
                </li>
                <li class="nav-item fix-menu">
                    <a class="nav-link" href="{{route('about-us')}}">{{trans('test.about')}}</a>
                </li>
                <li class="nav-item fix-menu">
                    <a class="nav-link" href="{{ route('product') }}">{{trans('test.products')}}</a>
                    <ul class="nav flex-column">
                        @foreach($product as $pro)
                            <li class="nav-item-fix">
                                <a class="nav-link" href="{{route('pro_detail',$pro->id)}}">{{$pro->name}}</a>
                            </li>
                        @endforeach
                    </ul>
                </li>
                <li class="nav-item fix-menu">
                    <a class="nav-link" href="{{route('news')}}">{{trans('test.news')}}</a>
                </li>
                <li class="nav-item fix-menu">
                    <a class="nav-link" href="{{route('contact')}}">{{trans('test.contact')}}</a>
                </li>
            </ul>
        </div>
    </nav>

</header>