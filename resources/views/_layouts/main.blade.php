<?php
/**
 * Created by PhpStorm.
 * User: dungnt
 * Date: 5/18/19
 * Time: 09:26
 */
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <title>VNPLUS</title>
    <base href="{{url('')}}/">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <!-- <link rel="stylesheet" href="owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="owlcarousel/owl.theme.default.min.css"> -->
    <link rel="stylesheet" href="assets/css/swiper.css">

    <script src="../jquery.min.js"></script>
    <script src="owlcarousel/owl.carousel.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style>
        /* Make the image fully responsive */
        .carousel-inner img {
            width: 100%;

        }
    </style>
</head>

<body>

@include('/_layouts/header')

@yield('content')

@include('/_layouts/footer')

<!-- Swiper JS -->


<!-- Initialize Swiper -->


</body>

</html>