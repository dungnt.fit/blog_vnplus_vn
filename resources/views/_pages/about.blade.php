@extends('/_layouts/main')
@section('content')
    <ul class="navbar-nav-2">
        @foreach(LaravelLocalization::getSupportedLocales() as $locale => $value)
            <li class="nav-item-2"><a class="nav-link-2" href="/{{$locale}}/about-us">
                    <img src="/assets/images/logo/{{$locale}}.jpg" width="20px" alt="">
                </a>
            </li>
        @endforeach
    </ul>
    <main>
        <section class="sec-7">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="txt_sec7">
                            <h2 style="color: #292723;text-transform: uppercase;text-shadow: rgba(29, 23, 24, 0.47) 0 1px 3px;padding: 20px 100px;">{{trans('test.about')}}</h2>
                            <div class="img_sec7">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="assets/images/our_factory/about-us-1.jpg" width="100%" height="100%" alt="">
                                    </div>
                                    <div class="col-md-4">
                                        <img src="assets/images/our_factory/about-us-2.jpg" width="100%" height="100%" alt="">
                                    </div>
                                    <div class="col-md-4">
                                        <img src="assets/images/our_factory/about-us-3.jpg" width="100%" height="100%" alt="">
                                    </div>
                                </div>
                            </div>
                            <span>{{trans('test.description-about')}}</span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="txt_sec7">
                            <h2 style="color: #292723;text-transform: uppercase;text-shadow: rgba(29, 23, 24, 0.47) 0 1px 3px;padding: 20px 100px;">{{trans('test.mission')}}</h2>
                            <ul style="padding: 0px">
                                <li style="list-style: none"><span>{{trans('test.mission-1')}}</span></li>
                                <li style="list-style: none"><span>{{trans('test.mission-2')}}</span></li>
                                <li style="list-style: none"><span>{{trans('test.mission-3')}}</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection