@extends('/_layouts/main')
@section('content')
    <ul class="navbar-nav-2">
        @foreach(LaravelLocalization::getSupportedLocales() as $locale => $value)
            <li class="nav-item-2"><a class="nav-link-2" href="/{{$locale}}/contact"><img src="/assets/images/logo/{{$locale}}.jpg"
                                                                      width="20px" alt=""></a></li>
        @endforeach
    </ul>
    <main>
        <section class="sec-9">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="map_sec9">
                            <h2 style="font-size: 29px; color: #292723;text-transform: uppercase;text-shadow: rgba(29, 23, 24, 0.47) 0 1px 3px;padding: 20px 100px;">{{trans('test.contact-information')}}</h2>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3725.1937602915677!2d105.78710951469301!3d20.984868794651717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135acc9486ee3e3%3A0xcecb10e0d83c2730!2zNTMgTEs2QiwgUC4gTeG7mSBMYW8sIEjDoCDEkMO0bmcsIEjDoCBO4buZaSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1560310243952!5m2!1svi!2s" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form_sec9">
                            <h2 style="font-size:29px; color: #292723;text-transform: uppercase;text-shadow: rgba(29, 23, 24, 0.47) 0 1px 3px;">{{trans('test.feed-back')}}</h2>
                            <form action="{{url('/contact')}}" method="post">
                                {{csrf_field()}}
                                <div class="form-group row">
                                    <div class="col-md-12 item_sec9">
                                        <input type="text" class="form-control" name="inputName" id="inputName" placeholder="{{trans('test.name')}}">
                                    </div>
                                    <div class="col-md-12 item_sec9">
                                        <input type="email" class="form-control" name="email" id="email" placeholder="{{trans('test.email')}}">
                                    </div>
                                    <div class="col-md-12 item_sec9">
                                        <input type="phone" class="form-control" name="phone" id="phone" placeholder="{{trans('test.telephone')}}">
                                    </div>
                                    <div class="col-md-12 item_sec9">
                                        <textarea class="form-control" placeholder="{{trans('test.content')}}" name="content" id="" cols="30" rows="5"></textarea>
                                    </div>
                                    <div class="col-md-12 item_sec9">
                                        <button style="text-transform: uppercase" type="submit" class="btn btn-primary">{{trans('test.send')}}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection