@extends('/_layouts/main')
@section('content')

    <ul class="navbar-nav-2">
        @foreach(LaravelLocalization::getSupportedLocales() as $locale => $value)

            <li class="nav-item-2">
                <a class="nav-link-2" href="/{{$locale}}">
                    <img src="/assets/images/logo/{{$locale}}.jpg" width="20px" alt="">
                </a>
            </li>
        @endforeach
    </ul>

    <nav>
        <div id="demo" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="assets/images/slide/banner2.jpg" alt="Los Angeles" >
                </div>
                <div class="carousel-item">
                    <img src="assets/images/slide/banner3.jpg" alt="Chicago" >
                </div>
                <div class="carousel-item">
                    <img src="assets/images/slide/banner4.jpg" alt="New York">
                </div>
            </div>
            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#demo" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a>
        </div>
    </nav>
    <main>
        <section class="sec-1">
            <div class="container-fuild">
                <div class="row">
                    <div class="col-md-1"></div>
                    @foreach($product as $pro)
                        <div class="col-md-2">
                            <div class="products">
                                <div class="products-item" style="transition: all 0.5s ease-out;">
                                    <div class="img_home" style="text-align: center">
                                            <div class="swiper-slide" ><img src="<?php $file_name = 'photos/'.json_decode($pro->featured_image)[0]; echo asset(Storage::url($file_name)); ?>" width="100%" alt=""></div>
                                    </div>
                                    <div class="txt_img">
                                        <h3 style="text-transform: capitalize">{{$pro->name}}</h3>
                                        <p>{{str_limit($pro->content,175)}}</p>
                                    </div>
                                    <div class="btn_img">
                                        <button><a href="{{route('pro_detail',$pro->id)}}">{{trans('test.read-more')}}</a></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-md-1"></div>
                </div>
            </div>
        </section>
        <section class="sec-2">
            <div class="container-fluid">
                <div class="txt_sec2">
                    <div class="container">
                        <h3>{{trans('test.title-1')}}</h3>
                        <p>{{trans('test.description-6')}}</p>
                        <hr>
                    </div>
                </div>
            </div>
        </section>
        <section class="sec-3">
            <div class="container">
                <h3>{{trans('test.our-factory')}}</h3>
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide" style="background-image:url(assets/images/our_factory/01.jpg)"></div>
                        <div class="swiper-slide" style="background-image:url(assets/images/our_factory/02.jpg)"></div>
                        <div class="swiper-slide" style="background-image:url(assets/images/our_factory/03.jpg)"></div>
                        <div class="swiper-slide" style="background-image:url(assets/images/our_factory/04.jpg)"></div>
                        <div class="swiper-slide" style="background-image:url(assets/images/our_factory/05.jpg)"></div>
                        <div class="swiper-slide" style="background-image:url(assets/images/our_factory/06.jpg)"></div>
                        <div class="swiper-slide" style="background-image:url(assets/images/our_factory/07.jpg)"></div>
                        <div class="swiper-slide" style="background-image:url(assets/images/our_factory/08.jpg)"></div>
                        <div class="swiper-slide" style="background-image:url(assets/images/our_factory/09.jpg)"></div>
                        <div class="swiper-slide" style="background-image:url(assets/images/our_factory/10.jpg)"></div>
                        <div class="swiper-slide" style="background-image:url(assets/images/our_factory/11.jpg)"></div>
                        <div class="swiper-slide" style="background-image:url(assets/images/our_factory/12.jpg)"></div>
                        <div class="swiper-slide" style="background-image:url(assets/images/our_factory/13.jpg)"></div>
                        <div class="swiper-slide" style="background-image:url(assets/images/our_factory/14.jpg)"></div>
                        <div class="swiper-slide" style="background-image:url(assets/images/our_factory/15.jpg)"></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="sec-2">
            <div class="container-fluid">
                <div class="txt_sec2">
                    <div class="container">
                        <h3>{{trans('test.our-pledge')}}</h3>
                        <ul style="padding: 0px">
                            <li style="list-style: none">{{trans('test.mission-1')}}</li>
                            <li style="list-style: none">{{trans('test.mission-2')}}</li>
                            <li style="list-style: none">{{trans('test.mission-3')}}</li>
                        </ul>
                        {{--<p>{{trans('test.description-7')}}</p>--}}
                        <hr>
                    </div>
                </div>
            </div>
        </section>
        {{--<section class="sec-4">--}}
        {{--<div class="container-fluid">--}}
        {{--<div class="txt_sec4">--}}
        {{--<div class="container">--}}
        {{--<div class="row">--}}
        {{--<div class="col-md-12">--}}
        {{--<div class="txt-sec2">--}}
        {{--<h3>{{trans('test.our-pledge')}}</h3>--}}
        {{--<p>{{trans('test.description-7')}}</p>--}}
        {{--<ul>--}}
        {{--<li>{{trans('test.quality-and-safety')}}</li>--}}
        {{--<li>{{trans('test.maximum-benefit')}}</li>--}}
        {{--<li>{{trans('test.ethnics')}}</li>--}}
        {{--<li>{{trans('test.environment')}}</li>--}}
        {{--</ul>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-md-4">--}}
        {{--<div class="row">--}}
        {{--<div class="col-md-12"><img src="/assets/images/pledge.jpg" width="100%" height="100%"--}}
        {{--alt=""></div>--}}
        {{--<div class="col-md-12">--}}
        {{--<div class="txt-sec4">--}}
        {{--<h5>{{trans('test.title-3')}}</h5>--}}
        {{--<p>--}}
        {{--{{trans('test.description-8')}}--}}
        {{--</p>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</section>--}}
        <section class="sec-5">
            <div class="container">
                <h3>{{trans('test.latest-news')}}</h3>
                <div class="row">
                    @foreach($news as $news_item)
                        <div class="col-md-3">
                            <div class="img_sec5 ">
                                <img src="assets/images/upload_image/{{$news_item->featured_image}}" width="100%" height="100%" alt="">
                            </div>
                            <div class="txt_sec5">
                                <h5><a href="{{route('news_detail',$news_item->id)}}" title="{{$news_item->news_title}}">{{str_limit($news_item->news_title,50)}}</a></h5>
                                <p>{{str_limit($news_item->news_content,100)}}</p>
                            </div>
                            <div class="btn_sec5">
                                <button style="outline: none"><a href="{{route('news_detail',$news_item->id)}}">{{trans('test.read-more')}}</a></button>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

        </section>
    </main>
    <script src="assets/js/swiper.js"></script>
    <script>
        var swiper = new Swiper('.swiper-container', {
            effect: 'coverflow',
            grabCursor: true,
            centeredSlides: true,
            slidesPerView: 'auto',
            coverflowEffect: {
                rotate: 50,
                stretch: 0,
                depth: 100,
                modifier: 1,
                slideShadows: true,
            },
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
            pagination: {
                el: '.swiper-pagination',
            },
        });

    </script>
@endsection
