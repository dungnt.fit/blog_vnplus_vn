@extends('/_layouts/main')
@section('content')
    <ul class="navbar-nav-2">
        @foreach(LaravelLocalization::getSupportedLocales() as $locale => $value)
            <li class="nav-item-2"><a class="nav-link-2" href="{{$locale}}/news">
                    <img src="/assets/images/logo/{{$locale}}.jpg" width="20px" alt="">
                </a>
            </li>
        @endforeach
    </ul>
    <main>
        <section class="sec-7">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="txt_sec7">
                            <h2 style="color: #292723;text-transform: uppercase;text-shadow: rgba(29, 23, 24, 0.47) 0 1px 3px;padding: 20px 100px;">{{trans('test.news')}}</h2>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            @foreach($news as $news_item)
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="img_sec8" style="border: 10px solid #4267b2; border-radius: 50px 0px 50px 0px">
                                            <img style="border-radius: 40px 0px 40px 0px" src="/assets/images/upload_image/{{$news_item->featured_image}}" width="100%" height="100%" alt="">
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="txt_sec8">
                                            <h4 style="font-size: 16px;"><a style="color: #a8a191; text-decoration: none" href="{{route('news_detail',$news_item->id)}}">{{str_limit($news_item->news_name,50)}}</a></h4>
                                            <p style="font-size: 12px">{{str_limit($news_item->news_content,150)}}</p>
                                            <a style="text-decoration : none; padding: 8px 15px; background-image: linear-gradient(to bottom, #f4643f, #f4a03f);border-radius: 10px" href="{{route('news_detail',$news_item->id)}}}">{{trans('test.read-more')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection