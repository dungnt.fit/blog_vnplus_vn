@extends('/_layouts/main')
@section('content')
    <ul class="navbar-nav-2">
        @foreach(LaravelLocalization::getSupportedLocales() as $locale => $value)
            <li class="nav-item-2"><a class="nav-link-2" href="/{{$locale}}/news/{{$news_detail->id}}">
                    <img src="/assets/images/logo/{{$locale}}.jpg" width="20px" alt="">
                </a>
            </li>
        @endforeach
    </ul>
    <main>
        <section class="sec-7">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="txt_sec7">

                            <h3 style="">{{$news_detail->news_title}}</h3>
                            <div class="img_sec7">
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-6">
                                        <img src="assets/images/upload_image/{{$news_detail->featured_image}}" width="100%" height="100%" alt="">
                                    </div>
                                </div>
                            </div>
                            <p>{{$news_detail->news_content}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection