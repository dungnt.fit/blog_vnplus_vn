@extends('/_layouts/main')
@section('content')
    <ul class="navbar-nav-2">
        @foreach(LaravelLocalization::getSupportedLocales() as $locale => $value)
        <li class="nav-item-2">
            <a class="nav-link-2" href="{{$locale}}/products/{{$pro_detail->id}}">
                <img src="/assets/images/logo/{{$locale}}.jpg" width="20px" alt="">
            </a>
        </li>
        @endforeach
    </ul>
    <main>
        <section class="sec-10">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h1>{{$pro_detail->description}}</h1>
                        <p>{{$pro_detail->content}}</p>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="slide_sec10">
                                    <div class="swiper-container gallery-top">
                                        <div class="swiper-wrapper img_sec10_top">
                                            @foreach(json_decode($pro_detail->featured_image) as $img_item)
                                                <div class="swiper-slide" ><img src="<?php $file_name = 'photos/'.$img_item; echo asset(Storage::url($file_name)); ?>" alt=""></div>
                                            @endforeach
                                            <div class="swiper-button-next swiper-button-white"></div>
                                            <div class="swiper-button-prev swiper-button-white"></div>
                                        </div>
                                        <div class="swiper-container gallery-thumbs">
                                            <div class="swiper-wrapper img_sec10_thumbs">
                                                @foreach(json_decode($pro_detail->featured_image) as $img_item)
                                                    <div class="swiper-slide" ><img src="<?php $file_name = 'photos/'.$img_item; echo asset(Storage::url($file_name)); ?>" alt=""></div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="container">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                {!!$pro_detail->pro_info!!}
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <!-- Swiper JS -->
    <script src="assets/js/swiper.js"></script>

    <!-- Initialize Swiper -->
    <script>
        var galleryThumbs = new Swiper('.gallery-thumbs', {
            spaceBetween: 10,
            slidesPerView: 4,
            loop: true,
            freeMode: true,
            loopedSlides: 5, //looped slides should be the same
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
        });
        var galleryTop = new Swiper('.gallery-top', {
            spaceBetween: 10,
            loop:true,
            loopedSlides: 5, //looped slides should be the same
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            thumbs: {
                swiper: galleryThumbs,
            },
        });
    </script>
@endsection