@extends('/_layouts/main')
@section('content')
    <ul class="navbar-nav-2">
        @foreach(LaravelLocalization::getSupportedLocales() as $locale => $value)
            <li class="nav-item-2"><a class="nav-link-2" href="{{$locale}}/products/"><img src="/assets/images/logo/{{$locale}}.jpg"
                                                                                                                               width="20px" alt=""></a></li>
        @endforeach
    </ul>
    <main>
        <section class="sec-8">
            <div class="container">
                <h2 style="color: #292723;text-transform: uppercase;text-shadow: rgba(29, 23, 24, 0.47) 0 1px 3px;padding: 20px 100px;">{{trans('test.products')}}</h2>
                <div class="row">
                    @foreach($product as $pro)
                    <div class="col-md-3">
                        <div class="img_sec8">
                            <img src="<?php $file_name = 'photos/'.json_decode($pro->featured_image)[0]; echo asset(Storage::url($file_name)); ?>" width="100%" alt="">
                        </div>
                        <div class="txt_sec8">
                            <h4><a href="{{route('pro_detail',$pro->id)}}">{{$pro->name}}</a></h4>
                            <p>{{$pro->content}}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
    </main>
    <!-- Swiper JS -->
    <script src="assets/js/swiper.js"></script>

    <!-- Initialize Swiper -->
    <script>
        var galleryThumbs = new Swiper('.gallery-thumbs', {
            spaceBetween: 10,
            slidesPerView: 4,
            loop: true,
            freeMode: true,
            loopedSlides: 5, //looped slides should be the same
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
        });
        var galleryTop = new Swiper('.gallery-top', {
            spaceBetween: 10,
            loop:true,
            loopedSlides: 5, //looped slides should be the same
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            thumbs: {
                swiper: galleryThumbs,
            },
        });
    </script>
@endsection