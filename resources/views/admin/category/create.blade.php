@extends('admin.layouts.master')
@section('content')
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            @foreach(LaravelLocalization::getSupportedLocales() as $locale => $value)
                <li class="{{ $locale == 'en' ? ' active' : '' }}">
                    <a href="#tab_{{ $locale }}" data-toggle="tab">{{ $value['name'] }}</a>
                </li>
            @endforeach
        </ul>
        <form action="{{ route('category.store') }}" enctype="multipart/form-data" method="post" style="margin-left: 15px; padding-bottom: 10px">
            <div class="tab-content">
                @foreach(LaravelLocalization::getSupportedLocales() as $locale => $value)
                    <div class="tab-pane {{ $locale == 'en' ? 'active' : '' }}" id="tab_{{ $locale }}">
                        <div class="form-group">
                            <label for="cat_name[{{ $locale }}]">Category Name</label>
                            <input type="text" class="form-control" id="cat_name[{{ $locale }}]" name="cat_name[{{ $locale }}]" placeholder="Enter name">
                        </div>
                        <div class="form-group">
                            <label for="cat_title[{{ $locale }}]">Category Title</label>
                            <input type="text" class="form-control" id="cat_title[{{ $locale }}]" name="cat_title[{{ $locale }}]" placeholder="Enter excerpt">
                        </div>
                        <div class="form-group">
                            <label for="cat_content[{{ $locale }}]">Category Content</label>
                            <input type="text" class="form-control" id="cat_content[{{ $locale }}]" name="cat_content[{{ $locale }}]" placeholder="Enter content">
                        </div>
                    </div>
                @endforeach
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </div>
            <!-- /.tab-content -->
            @csrf
        </form>

    </div>
@endsection