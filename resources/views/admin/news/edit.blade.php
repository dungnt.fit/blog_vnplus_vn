@extends('admin.layouts.master')
@section('content')
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            @foreach(LaravelLocalization::getSupportedLocales() as $locale => $value)
                <li class="{{ $locale == 'en' ? ' active' : '' }}">
                    <a href="#tab_{{ $locale }}" data-toggle="tab">{{ $value['name'] }}</a>
                </li>
            @endforeach
        </ul>
        <form action="{{route('news.update',['id'=>$news->id])}}" enctype="multipart/form-data" method="post" style="margin-left: 15px; padding-bottom: 10px">
            <div class="tab-content">
                @foreach(LaravelLocalization::getSupportedLocales() as $locale => $value)
                    <div class="tab-pane {{ $locale == 'en' ? 'active' : '' }}" id="tab_{{ $locale }}">
                        <div class="form-group">
                            <label for="news_name[{{ $locale }}]">News Name</label>
                            <input type="text" class="form-control" id="news_name[{{ $locale }}]" name="news_name[{{ $locale }}]" placeholder="Enter News name" value="{{$locale == 'en' ? $news['translations'][0]->news_name : $news['translations'][1]->news_name}}">
                        </div>
                        <div class="form-group">
                            <label for="news_title[{{ $locale }}]">News Title</label>
                            <input type="text" class="form-control" id="news_title[{{ $locale }}]" name="news_title[{{ $locale }}]" placeholder="Enter news title" value="{{$locale == 'en' ? $news['translations'][0]->news_title : $news['translations'][1]->news_title}}">
                        </div>
                        <div class="form-group">
                            <label for="news_content[{{ $locale }}]">News Content</label>
                            <input type="text" class="form-control" id="news_content[{{ $locale }}]" name="news_content[{{ $locale }}]" placeholder="Enter news content" value="{{$locale == 'en' ? $news['translations'][0]->news_content : $news['translations'][1]->news_content}}">
                        </div>
                    </div>
                @endforeach
                <div class="form-group">
                    <label for="featured_image">image</label>
                    <input type="file" class="form-control" name="image" placeholder="Enter content">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
            <!-- /.tab-content -->
            @csrf
        </form>

    </div>
@endsection