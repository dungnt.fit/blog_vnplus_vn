@extends('admin.layouts.master')
@section('content')
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            @foreach(LaravelLocalization::getSupportedLocales() as $locale => $value)
                <li class="{{ $locale == 'en' ? ' active' : '' }}">
                    <a href="#tab_{{ $locale }}" data-toggle="tab">{{ $value['name'] }}</a>
                </li>
            @endforeach
        </ul>
        <form action="{{ route('product.store') }}" enctype="multipart/form-data" method="post" style="margin-left: 15px; padding-bottom: 10px">
        <div class="tab-content">
            @foreach(LaravelLocalization::getSupportedLocales() as $locale => $value)
                <div class="tab-pane {{ $locale == 'en' ? 'active' : '' }}" id="tab_{{ $locale }}">
                    <div class="form-group">
                        <label for="name[{{ $locale }}]">Name</label>
                        <input type="text" class="form-control" id="name[{{ $locale }}]" name="name[{{ $locale }}]" placeholder="Enter name">
                    </div>
                    <div class="form-group">
                        <label for="excerpt[{{ $locale }}]">Excerpt</label>
                        <input type="text" class="form-control" id="excerpt[{{ $locale }}]" name="excerpt[{{ $locale }}]" placeholder="Enter excerpt">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <input type="text" class="form-control" id="description[{{ $locale }}]" name="description[{{ $locale }}]" placeholder="Enter description">
                    </div>
                    <div class="form-group">
                        <label for="content[{{ $locale }}]">Content</label>
                        <input type="text" class="form-control" id="content[{{ $locale }}]" name="content[{{ $locale }}]" placeholder="Enter content">
                    </div>
                    <div class="form-group">
                        <label for="pro_info[{{ $locale }}]">Product Info</label>
                        <input type="text" class="form-control" id="pro_info[{{ $locale }}]" name="pro_info[{{ $locale }}]" placeholder="Enter pro_info">
                    </div>
                </div>
            @endforeach
                <div class="form-group">
                    <label for="featured_image">image</label>
                    <input type="file" class="form-control" name="image[]" placeholder="Enter content" id="file_upload" multiple="multiple">
                    <input type="text" id="image_upload" style="display: none">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
        </div>
        <!-- /.tab-content -->
            @csrf
        </form>

    </div>
@endsection

