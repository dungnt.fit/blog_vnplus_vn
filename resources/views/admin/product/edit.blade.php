@extends('admin.layouts.master')
@section('content')

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">

            @foreach(LaravelLocalization::getSupportedLocales() as $locale => $value)
                <li class="{{ $locale == 'en' ? ' active' : '' }}">
                    <a href="#tab_{{ $locale }}" data-toggle="tab">{{ $value['name'] }}</a>
                </li>
            @endforeach
        </ul>
        <form action="{{route('product.update',['id'=>$product->id])}}" enctype="multipart/form-data" method="post" style="margin-left: 15px; padding-bottom: 10px">
            <div class="tab-content">
                @foreach(LaravelLocalization::getSupportedLocales() as $locale => $value)
                    <div class="tab-pane {{ $locale == 'en' ? 'active' : '' }}" id="tab_{{ $locale }}">
                        <div class="form-group">
                            <label for="name[{{ $locale }}]">Name</label>
                            <input type="text" class="form-control" id="name[{{ $locale }}]" name="name[{{ $locale }}]" placeholder="Enter name" value="{{$locale == 'en' ? $product['translations'][0]->name : $product['translations'][1]->name}}">
                        </div>
                        <div class="form-group">
                            <label for="excerpt[{{ $locale }}]">Excerpt</label>
                            <input type="text" class="form-control" id="excerpt[{{ $locale }}]" name="excerpt[{{ $locale }}]" placeholder="Enter excerpt" value="{{$locale == 'en' ? $product['translations'][0]->excerpt : $product['translations'][1]->excerpt}}">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <input type="text" class="form-control" id="description[{{ $locale }}]" name="description[{{ $locale }}]" placeholder="Enter description" value="{{$locale == 'en' ? $product['translations'][0]->description : $product['translations'][1]->description}}">
                        </div>
                        <div class="form-group">
                            <label for="content[{{ $locale }}]">Content</label>
                            <input type="text" class="form-control" id="content[{{ $locale }}]" name="content[{{ $locale }}]" placeholder="Enter content" value="{{$locale == 'en' ? $product['translations'][0]->content : $product['translations'][1]->content}}">
                        </div>
                        <div class="form-group">
                            <label for="pro_info[{{ $locale }}]">Product Info</label>
                            <input type="text" class="form-control" id="pro_info[{{ $locale }}]" name="pro_info[{{ $locale }}]" placeholder="Enter pro_info" value="{{$locale == 'en' ? $product['translations'][0]->pro_info : $product['translations'][1]->pro_info}}">
                        </div>
                    </div>
                @endforeach

                <div class="form-group">
                    <label for="featured_image">image</label>
                    <div class="row" style="text-align: center">
                        @foreach(json_decode($product->featured_image) as $img_item)
                            <div class="col-md-2" style="padding-bottom: 20px"><img src="<?php $file_name = 'photos/'.$img_item; echo asset(Storage::url($file_name)); ?>" width="100px" height="100px" alt=""></div>
                        @endforeach
                    </div>
                    <input type="file" class="form-control" name="image[]" placeholder="Enter content" multiple="multiple">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
            <!-- /.tab-content -->
            @csrf
        </form>

    </div>
@endsection