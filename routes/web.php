<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//frontend da ngon ngu
Route::group(['prefix' => LaravelLocalization::setLocale()],function (){
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/about-us','AboutController@showAbout')->name('about-us');
    Route::get('/products','ProductsController@showProducts')->name('product');
    Route::get('news','NewsController@showNews')->name('news');
    Route::get('/products/pepper','PepperController@showPepper');
    Route::get('/products/roasted-coffee','RoastedController@showRoasted');
    Route::get('/products/coffee-beans','BeansController@showBeans');
    Route::get('/products/canned-pineapple','PineappleController@showPineapple');
    Route::get('/products/canned-cucumber','CucumberController@showCucumber');
    Route::get('/contact','ContactController@showContact')->name('contact');
    Route::post('/contact','ContactController@store');
    Route::get('/products/{id}','ProductDetailController@index')->name('pro_detail');
    Route::get('/news/{id}','NewsDetailController@index')->name('news_detail');
});

Auth::routes();





Route::group(['middleware' => 'auth','prefix' => 'admin','namespace' => 'Admin'], function(){
    Route::get('/','DashboardController@index')->name('dashboard');

    // Product

    Route::group(['prefix' => 'product'], function(){
        Route::get('add-new', [
            'as' => 'product.add',
            'uses' => 'ProductController@create'
        ]);

        Route::get('list', [
            'as' => 'product.list',
            'uses' => 'ProductController@index'
        ]);

        Route::post('store', [
            'as' => 'product.store',
            'uses' => 'ProductController@store'
        ]);
        Route::get('edit/{id}',[
            'as'=>'product.edit',
            'uses' => 'ProductController@edit'
        ]);
        Route::post('update/{id}',[
            'as'=>'product.update',
            'uses' => 'ProductController@update'
        ]);
        Route::get('destroy/{id}',[
            'as'=>'product.destroy',
            'uses' => 'ProductController@destroy'
        ]);
    });
    Route::group(['prefix'=>'category'],function (){
        Route::get('add-new','CategoryController@create')->name('category.add');
        Route::post('store','CategoryController@store')->name('category.store');
        Route::get('edit/{id}','CategoryController@edit')->name('category.edit');
        Route::post('update/{id}','CategoryController@update')->name('category.update');
        Route::get('destroy/{id}','CategoryController@destroy')->name('category.destroy');
        Route::get('list','CategoryController@index')->name('category.list');
    });
    Route::group(['prefix'=>'news'],function (){
        Route::get('add-new','NewsController@create')->name('news.add');
        Route::post('store','NewsController@store')->name('news.store');
        Route::get('edit/{id}','NewsController@edit')->name('news.edit');
        Route::post('update/{id}','NewsController@update')->name('news.update');
        Route::get('destroy/{id}','NewsController@destroy')->name('news.destroy');
        Route::get('list','NewsController@index')->name('news.list');
    });
});
//Route::post('/file/dropzone','FileController@dropzone')->name('dropzone');
//Route::get('/','ImageController@getUpload');
//Route::post('upload', 'ImageController@postUpload');
//Route::delete('upload-delete','ImageController@deleteUpload');
